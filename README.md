# fundapps-test


Requires SSL Certificate to be present in working region for domain: test.kifvel.com

Application has its own repository for seperate pipeline: https://gitlab.com/toderov/fundapps-test-app


Created simple web app to display test in browser. App consists of flask running in a docker container.

DNS A record points to ALB DNS which serves HTTPS to client.
ECS Fargate cluster manages containers via target group & ASG.

Ideally looking to manage targets via CPU/Memory however for this case looking to set monitoring of requests to create/remove tasks within cluster.

Terraform config incomplete - no time for ASG/monitoring. - update ASG & CW alarms configured & tested.



Looking to create pipeline for terraform:
    Changes made via pull/merge requests - repo locked to only allow merge requests via approval.
    Set AWS keys as Gitlab variables.
    Run terraform image to init, validate & apply changes. Each in own stage of build, test & deploy.

Pipeline for Application repository:
    Changes locked to merge/pull requests via approval only.
    build stage using docker image to create new container
    test stage, docker image, to run the container, check to 200 status.
    deploy stage, docker image, to push the container to the dockerhub.
    deploy stage, aws image, run aws cli to update the service in order for service trigger tasks to retrieve updated docker container in round-robin update process.
