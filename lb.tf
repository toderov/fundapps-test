resource "aws_lb" "fundapps-lb" {
  name               = "fundapps-lb"
  subnets            = data.aws_subnet_ids.subnet-id.ids
  load_balancer_type = "application"
  security_groups    = [aws_security_group.fundapps-sg-alb.id]
  internal           = false

  tags = {
    Name = var.tag
  }
}

resource "aws_lb_listener_certificate" "cert" {
  listener_arn    = aws_lb_listener.https_forward.arn
  certificate_arn = data.aws_acm_certificate.cert.arn
}

resource "aws_lb_listener" "https_forward" {
  load_balancer_arn = aws_lb.fundapps-lb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = data.aws_acm_certificate.cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.fundapps-tg.arn
  }
}

resource "aws_lb_listener" "http-redirect" {
  load_balancer_arn = aws_lb.fundapps-lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group" "fundapps-tg" {
  name        = "fundapps-tg"
  port        = 8888
  protocol    = "HTTP"
  vpc_id      = aws_vpc.fundapps_vpc.id
  target_type = "ip"

  # health_check {
  #   healthy_threshold   = "3"
  #   interval            = "10"
  #   protocol            = "HTTP"
  #   matcher             = "200-299"
  #   timeout             = "20"
  #   path                = "/health-check"
  #   unhealthy_threshold = "2"
  # }
  tags = {
    Name = var.tag
  }
}