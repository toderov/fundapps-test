# auto_scaling.tf

resource "aws_appautoscaling_target" "target" {
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.fundapps-demo-cluster.name}/${aws_ecs_service.fundapps-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  role_arn           = data.aws_iam_role.EcsAutoScaleRole.arn
  min_capacity       = 2
  max_capacity       = 4
}

# Automatically scale capacity up by one
resource "aws_appautoscaling_policy" "up" {
  name               = "scale_up"
  policy_type        = "StepScaling"
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.fundapps-demo-cluster.name}/${aws_ecs_service.fundapps-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment = 1

    }
  }
  # # Target tracking spec
  # predefined_metric_specification {
  #     predefined_metric_type = "ALBRequestCountPerTarget"
  #     resource_label         = "${aws_lb.fundapps-lb.arn_suffix}/${aws_lb_target_group.fundapps-tg.arn_suffix}"
  #   }
  # }

  depends_on = [aws_appautoscaling_target.target]
}

# Automatically scale capacity down by one
resource "aws_appautoscaling_policy" "down" {
  name               = "scale_down"
  policy_type        = "StepScaling"
  service_namespace  = "ecs"
  resource_id        = "service/${aws_ecs_cluster.fundapps-demo-cluster.name}/${aws_ecs_service.fundapps-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment = -1
    }
  }

  depends_on = [aws_appautoscaling_target.target]
}

# CloudWatch alarm that triggers the autoscaling up policy
resource "aws_cloudwatch_metric_alarm" "requests_high" {
  alarm_name          = "requests_high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Sum"
  threshold           = "10"

  dimensions = {
    TargetGroup  = aws_lb_target_group.fundapps-tg.arn_suffix
    LoadBalancer = aws_lb.fundapps-lb.arn_suffix
  }

  alarm_actions = [aws_appautoscaling_policy.up.arn]
}


# CloudWatch alarm that triggers the autoscaling down policy
resource "aws_cloudwatch_metric_alarm" "requests_low" {
  alarm_name          = "requests_low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Sum"
  threshold           = "9"

  dimensions = {
    TargetGroup  = aws_lb_target_group.fundapps-tg.arn_suffix
    LoadBalancer = aws_lb.fundapps-lb.arn_suffix
  }

  alarm_actions = [aws_appautoscaling_policy.down.arn]
}


resource "aws_cloudwatch_log_group" "fundapps-demo-logs" {
  name = "ecs/fundapps-demo-logs"
}


resource "aws_cloudwatch_log_stream" "fundappslog-stream" {
  name           = "fundapps-log-stream"
  log_group_name = aws_cloudwatch_log_group.fundapps-demo-logs.name
}