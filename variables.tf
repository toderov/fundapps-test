variable "region" {
  default = "us-east-1"
}
variable "tag" {
  default = "fundapps-demo"
}

variable "project" {
  default = "fundapps"
}

variable "subnet_cidrs_public" {
  description = "Subnet CIDRs for public subnets (length must match configured availability_zones)"
  default     = ["10.0.10.0/24", "10.0.20.0/24"]
  type        = list(any)
}

variable "availability_zones" {
  description = "AZs in this region to use"
  default     = ["us-east-1a", "us-east-1b"]
  type        = list(any)
}

variable "ecs_task_execution_role" {
  description = "ECS task execution role name"
  default     = "myEcsTaskExecutionRole"
}

variable "ecs_auto_scale_role" {
  description = "ECS auto scale role Name"
  default     = "EcsAutoScaleRole"
}


data "aws_subnet_ids" "subnet-id" {
  vpc_id     = aws_vpc.fundapps_vpc.id
  depends_on = [aws_subnet.fundapps-subnet]
}

# Find a certificate that is issued
data "aws_acm_certificate" "cert" {
  domain = "test.kifvel.com"
  # types    = ["IMPORTED"]
  statuses = ["ISSUED"]
}

data "aws_iam_role" "EcsAutoScaleRole" {
  name = "EcsAutoScaleRole"
}