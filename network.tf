# Networking

resource "aws_vpc" "fundapps_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = var.tag
  }
}


# resource "aws_subnet" "fundapps-subnet1" {
#   vpc_id     = aws_vpc.fundapps_vpc.id
#   cidr_block = "10.0.10.0/24"

#   tags = {
#     Name = var.tag
#   }
# }


# resource "aws_subnet" "fundapps-subnet2" {
#   vpc_id     = aws_vpc.fundapps_vpc.id
#   cidr_block = "10.0.20.0/24"

#   tags = {
#     Name = var.tag
#   }
# }

resource "aws_internet_gateway" "fundapps-gw" {
  vpc_id = aws_vpc.fundapps_vpc.id

  tags = {
    Name = var.tag
  }
}


resource "aws_route_table" "fundapps-rt" {
  vpc_id = aws_vpc.fundapps_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.fundapps-gw.id
  }

  tags = {
    Name = var.tag
  }
}

resource "aws_subnet" "fundapps-subnet" {
  count = length(var.subnet_cidrs_public)

  vpc_id            = aws_vpc.fundapps_vpc.id
  cidr_block        = var.subnet_cidrs_public[count.index]
  availability_zone = var.availability_zones[count.index]
}

resource "aws_route_table_association" "public" {
  count = length(var.subnet_cidrs_public)

  subnet_id      = element(aws_subnet.fundapps-subnet.*.id, count.index)
  route_table_id = aws_route_table.fundapps-rt.id
}



# resource "aws_route_table_association" "rt-association" {
#   subnet_id      = aws_subnet.fundapps-subnet1.id
#   route_table_id = aws_route_table.fundapps-rt.id
# }


# Security Groups

resource "aws_security_group" "fundapps-sg-alb" {
  name        = "fundapps-sg-alb"
  vpc_id      = aws_vpc.fundapps_vpc.id
  description = "controls access to the Application Load Balancer (ALB)"

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = var.tag
  }
}

resource "aws_security_group" "fundapps-sg-tasks" {
  name        = "fundapps-sg-tasks"
  vpc_id      = aws_vpc.fundapps_vpc.id
  description = "allow inbound access from the ALB only"

  ingress {
    protocol  = "tcp"
    from_port = 8888
    to_port   = 8888
    # cidr_blocks     = ["0.0.0.0/0"]
    security_groups = [aws_security_group.fundapps-sg-alb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = var.tag
  }
}