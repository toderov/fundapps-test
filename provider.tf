terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

terraform {
  backend "http" {
  }
}

# - terraform init 
#    address=${GITLAB_TF_ADDRESS}" 
#    lock_address=${GITLAB_TF_ADDRESS}/lock" 
#    unlock_address=${GITLAB_TF_ADDRESS}/lock" 
#    username=gitlab-ci-token" 
#    password=${CI_JOB_TOKEN}" 
#    lock_method=POST" 
#    unlock_method=DELETE" 
#    retry_wait_min=5"

