resource "aws_cloudwatch_log_group" "fundapps" {
  name = "fundapps-demo-logs"
}


resource "aws_ecs_cluster" "fundapps-demo-cluster" {
  name = "fundapps-cluster"
  setting {
    name = "containerInsights"
    value = "enabled"
  }

  tags = {
    Name = var.tag
  }

}


resource "aws_ecs_task_definition" "fundapp-task-def" {
  family                   = "fundapps-app-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 512
  memory                   = 1024
  container_definitions    = file("cont-def.json")
}


resource "aws_ecs_service" "fundapps-service" {
  name            = "fundapps-service"
  cluster         = aws_ecs_cluster.fundapps-demo-cluster.id
  task_definition = aws_ecs_task_definition.fundapp-task-def.arn
  desired_count   = 2
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.fundapps-sg-tasks.id]
    subnets          = data.aws_subnet_ids.subnet-id.ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.fundapps-tg.id
    container_name   = "fundapps-flask-app"
    container_port   = 8888
  }

  depends_on = [aws_lb_listener.https_forward, aws_iam_role_policy_attachment.ecs_task_execution_role]
}

# add autoscaling config group